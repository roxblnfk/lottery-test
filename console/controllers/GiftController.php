<?php

namespace console\controllers;

use common\models\Gift;
use common\models\Thing;
use common\models\User;
use common\services\GiftService;
use yii\console\Controller;
use yii\console\Exception;
use yii\db\ActiveQuery;

class GiftController extends Controller
{
    /**
     * Добавить новый вещевой подарок с активным статусом
     * @param int|string $userId
     * @param int|string $type
     * @param int        $value
     * @throws Exception
     */
    public function actionAdd($userId, $type, $value)
    {
        # Поиск пользователя
        $findById = preg_match('/^[0-9]$/', $userId);
        /** @var User|null $user */
        $user = User::find()->where($findById ? ['id' => $userId] : ['username' => $userId])->one();
        if (!$user) {
            throw new Exception("Пользователь {$userId} не найден");
        }
        $type = (int)$type;

        if ($type === Gift::TYPE_THING) {
            # Поиск товара
            $giftById = preg_match('/^[0-9]$/', $userId);
            if (!$giftById) {
                throw new Exception('В поле Value укажите числовой ID подарка');
            }
            /** @var Thing|null $thing */
            $thing = Thing::find()->allowedForGift()
                          ->andWhere(['id' => $value])
                          ->one();
            if (!$thing) {
                throw new Exception("Подарок #{$value} не найден / не активен / закончился");
            }
            echo "Подарок: {$thing->title}\n";
            $value = $thing;
        }

        $giftService = new GiftService();
        $gift = $giftService->createGift($user, $type, $value);

        echo $gift ? 'ok id:' . $gift->id : 'fail';
    }


    /**
     * Отправить денежные подарки пользователям на банковские счета
     */
    public function actionSendMoney()
    {
        $query = Gift::find()->statusWaiting()
                             ->andWhere(['type' => Gift::TYPE_MONEY])
                             ->innerJoinWith(['user' => function (ActiveQuery $q) {
                                 $q->where(['not', ['bank_account' => null]]);
                             }], true);
        $giftService = new GiftService();

        # todo: сгруппировать по пользователю/номеру_чёта и закрывать сразу несколько денежных призов
        foreach ($query->each() as $gift) {
            /** @var Gift $gift */
            $user = $gift->user;
            echo "- {$user->username} : {$gift->money_amount} -> {$user->bank_account}\n";
            # todo
            // $giftService->resolveGift($gift, new common\actions\gift\MoneyToBankAction($user->bank_account));
        }
    }
}
