<?php

namespace console\controllers;

use common\models\Thing;
use common\services\GiftService;
use yii\console\Controller;
use yii\console\Exception;

class ThingController extends Controller
{
    /**
     * Добавить новый вещевой подарок с активным статусом
     * @param string $title
     * @param int    $count Кол-во доступных товаров
     */
    public function actionAdd($title, $count)
    {
        $thing = new Thing();
        $thing->title = $title;
        $thing->active = true;
        $thing->count_allowed = (int)$count;

        $saved = $thing->save();

        echo $saved ? 'ok id:' . $thing->id : 'fail';
    }

    /**
     * Получить список разыгрываемых вещей
     */
    public function actionActiveList()
    {
        $giftService = new GiftService();
        $listQuery = Thing::find()->allowedForGift();

        foreach ($listQuery->each() as $row) {
            /** @var $row Thing */
            echo "{$row->id} - {$row->title} [{$row->count_allowed} / {$row->count_reserved}]\n";
        }
        echo 'Всего единиц товара: ' . $giftService->getAllowedThingsCount();
    }
}
