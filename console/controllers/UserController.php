<?php

namespace console\controllers;

use common\models\User;
use yii\console\Controller;
use yii\console\Exception;

class UserController extends Controller
{
    /**
     * Добавить нового пользователя
     * @param string $login    Login
     * @param string $email
     * @param string $password Password
     * @throws Exception
     */
    public function actionAdd($login, $email, $password)
    {
        echo "{$login}:{$email}:{$password}\n";

        if (User::find()->where(['username' => $login])->one())
            throw new Exception('Логин занят');

        $user = new User();
        $user->email = $email;
        $user->username = $login;
        $user->setPassword($password);
        $user->generateAuthKey();
        $user->status = User::STATUS_ACTIVE;

        $saved = $user->save();

        echo $saved ? 'ok id:' . $user->id : 'fail';
    }

    /**
     * Найти пользователя по логину
     * @param string $login Login
     * @throws Exception
     */
    public function actionFind($login)
    {
        $user = User::find()->where(['username' => $login])->one();
        if (!$user)
            throw new Exception('Пользователь не найден');

        echo "id: {$user->id}\n";
        echo "created: " . date('r', $user->created_at) . "\n";
        echo "updated: " . date('r', $user->updated_at) . "\n";
    }
}
