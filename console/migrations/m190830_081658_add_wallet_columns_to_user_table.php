<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user}}`.
 */
class m190830_081658_add_wallet_columns_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'bank_account', $this->string(64)->defaultValue(null)->comment('Условный банковский счёт'));
        $this->addColumn('{{%user}}', 'bonuses', $this->integer()->unsigned()->defaultValue(0)->comment('Сумма бонусных баллов'));
        $this->addColumn('{{%user}}', 'post_address', $this->string(255)->defaultValue(null)->comment('Адрес доставки'));
        $this->renameColumn('{{%user_gift}}', 'status', 'state');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'bank_account');
        $this->dropColumn('{{%user}}', 'bonuses');
        $this->dropColumn('{{%user}}', 'post_address');
        $this->renameColumn('{{%user_gift}}', 'state', 'status');
    }
}
