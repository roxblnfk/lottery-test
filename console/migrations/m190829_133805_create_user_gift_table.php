<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_gift}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%gift}}`
 */
class m190829_133805_create_user_gift_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_gift}}', [
            'id'           => $this->primaryKey(),
            'user_id'      => $this->integer()->notNull(),
            'type'         => $this->integer()->notNull()->comment('Тип подарка (деньги, баллы, вещь)'),
            'money_amount' => $this->integer()->defaultValue(null)->comment('Кол-во денег'),
            'ball_amount'  => $this->integer()->defaultValue(null)->comment('Кол-во баллов'),
            'thing_id'      => $this->integer()->defaultValue(null)->comment('ID вещевого подарка'),
            'status'       => $this->tinyInteger()
                                   ->notNull()
                                   ->defaultValue(0)
                                   ->comment('Статус подарка (ожидание решения от пользователя/в процессе/принят)'),
            'action'       => $this->integer()->notNull()->defaultValue(0)->comment('Действие (решение) пользователя'),
            'action_at'    => $this->datetime()->defaultValue(null)->comment('Время принятия решения'),
            'action_prop'  => $this->string()->defaultValue(null)->comment('Параметры')->comment('Параметры события при принятии решения'),
            'created_at'   => $this->datetime()->notNull()->comment('Время получения (создания) подарка'),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-user_gift-user_id}}',
            '{{%user_gift}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-user_gift-user_id}}',
            '{{%user_gift}}',
            'user_id',
            '{{%user}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );

        // creates index for column `thing_id`
        $this->createIndex(
            '{{%idx-user_gift-thing_id}}',
            '{{%user_gift}}',
            'thing_id'
        );

        // add foreign key for table `{{%gift}}`
        $this->addForeignKey(
            '{{%fk-user_gift-gift_thing_id}}',
            '{{%user_gift}}',
            'thing_id',
            '{{%gift_thing}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-user_gift-user_id}}',
            '{{%user_gift}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-user_gift-user_id}}',
            '{{%user_gift}}'
        );

        // drops foreign key for table `{{%gift}}`
        $this->dropForeignKey(
            '{{%fk-user_gift-gift_thing_id}}',
            '{{%user_gift}}'
        );

        // drops index for column `thing_id`
        $this->dropIndex(
            '{{%idx-user_gift-thing_id}}',
            '{{%user_gift}}'
        );

        $this->dropTable('{{%user_gift}}');
    }
}
