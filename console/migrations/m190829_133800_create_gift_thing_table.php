<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%gift_thing}}`.
 */
class m190829_133800_create_gift_thing_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%gift_thing}}', [
            'id'             => $this->primaryKey(),
            'title'          => $this->string()->notNull(),
            'active'         => $this->tinyInteger()->defaultValue(0)->comment('Товар участвует в розыгрыше'),
            'count_allowed'  => $this->integer()
                                     ->notNull()
                                     ->unsigned()
                                     ->defaultValue(0)
                                     ->comment('Кол-во товара, выделенное для розыгрыша'),
            'count_reserved' => $this->integer()
                                     ->notNull()
                                     ->unsigned()
                                     ->defaultValue(0)
                                     ->comment('Кол-во зарезервированного товара (разыграно)'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%gift_thing}}');
    }
}
