<?php

namespace common\services;

class DummyBankAPI
{
    /**
     * @param int    $moneyCount
     * @param string $bankAccount
     * @return bool
     */
    public function sendMoney($moneyCount, $bankAccount)
    {
        return true;
    }
}