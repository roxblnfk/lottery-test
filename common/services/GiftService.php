<?php


namespace common\services;


use common\models\Gift;
use common\models\Thing;
use common\models\User;
use Exception;

class GiftService
{
    /** @var int[] мин. и макс. значения денежного подарка */
    public $limMoney = [85, 100];
    /** @var int[] мин. и макс. значения подарка в баллах */
    public $limBall = [85, 100];

    /** @var \Throwable[] */
    protected $errors = [];

    /**
     * Создать и привязать подарок указанного типа и номинала пользователю
     * @param User      $user
     * @param int       $type
     * @param int|Thing $value
     * @return Gift|false
     */
    public function createGift(User $user, $type, $value)
    {
        try {
            switch ($type) {

                # подарить баллы
                case Gift::TYPE_BALL:
                    $gift = new Gift(['ball_amount' => $value]);
                    break;

                # подарить деньги
                case Gift::TYPE_MONEY:
                    if ($this->getAllowedMoney() < $value) {
                        throw new Exception('No money exception');
                    }
                    $gift = new Gift(['money_amount' => $value]);
                    break;

                # подарить вещь
                case Gift::TYPE_THING:
                    if (!$value instanceof Thing) {
                        throw new Exception('Bad value exception');
                    }
                    $gift = new Gift(['thing_id' => $value->id]);
                    break;

                default:
                    throw new Exception('Bad gift type exception');
            }

            $gift->type = $type;
            $gift->created_at = date("Y-m-d H:i:s");
            $gift->user_id = $user->id;

            # запись в БД в транзакции
            $db = \Yii::$app->db;
            $transaction = $db->beginTransaction();

            try {
                $saved = $gift->save();
                if ($saved) {
                    # в случае дарения денег/вещей бронировать их в соответствующих местах
                    if ($type === Gift::TYPE_MONEY) {
                        # TODO: бронировать сумму денег
                    } elseif ($type === Gift::TYPE_THING) {
                        $value->updateCounters(['count_reserved' => 1]);
                    }
                }

                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollback();
                throw $e;
            }
        } catch (\Throwable $e) {
            $this->errors[] = $e;
            echo $e->getMessage();
            return false;
        }

        return $gift;
    }

    /**
     * Создать и привязать случайный подарок пользователю
     * @param User $user
     * @return Gift
     */
    public function createRandomGift(User $user)
    {
        $allowedMoney = $this->getAllowedMoney();
        $allowedThings = $this->getAllowedThingsCount();

        # 0 - balls; 1 - money; 2 - thing
        $toRand = [Gift::TYPE_BALL];
        if ($allowedMoney > 0 && $allowedMoney >= $this->limMoney[0]) $toRand[] = Gift::TYPE_MONEY;
        if ($allowedThings > 0) $toRand[] = Gift::TYPE_THING;

        # розыгрыш типа подарка
        $giftType = $toRand[rand(0, count($toRand) - 1)];

        switch ($giftType) {
            case Gift::TYPE_MONEY:
                $value = rand($this->limMoney[0], $this->limMoney[1]);
                break;
            case Gift::TYPE_THING:
                $value = Thing::find()->allowedForGift()->getRandom();
                break;
            default:
                $value = rand($this->limBall[0], $this->limBall[1]);
                break;
        }

        return $this->createGift($user, $giftType, $value);
    }

    /**
     * Todo
     * @return int
     */
    public function getAllowedMoney()
    {
        return 1000;
    }

    /**
     * Получить количество оставшегося разыгрываемого товара
     * @return int
     */
    public function getAllowedThingsCount()
    {
        return Thing::find()->allowedForGift()->sumAvailableThings();
    }

    /**
     * @return \Throwable[]
     */
    public function getErrors()
    {
        return $this->errors;
    }
}