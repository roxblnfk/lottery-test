<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%user_gift}}".
 *
 * @property int    $id
 * @property int    $user_id
 * @property int    $type         Тип подарка (деньги, баллы, товар)
 * @property int    $money_amount Кол-во денег
 * @property int    $ball_amount  Кол-во баллов
 * @property int    $thing_id     ID товара
 * @property int    $state        Статус подарка
 * @property int    $action       Действие (решение) пользователя
 * @property string $action_at    Время принятия решения
 * @property string $action_prop  Параметры события при принятии решения
 * @property string $created_at   Время получения (создания) подарка
 *
 * @property Thing  $thing
 * @property User   $user
 *
 * TODO: Добавить поведение для полей created_at, action_at
 */
class Gift extends \yii\db\ActiveRecord
{
    const TYPE_MONEY = 1;
    const TYPE_BALL  = 2;
    const TYPE_THING = 4;

    const STATE_WAITING    = 0;    # Ожидание решения от пользователя
    const STATE_PROCESSING = 1; # В процессе
    const STATE_SOLVED     = 2;     # Жизненный цикл подарка завершен

    const ACTION_REJECT = 1;    # Действие отказа от подарка
    const ACTION_ACCEPT = 2;    # Действие к зачислению подарка

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_gift}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'type', 'created_at'], 'required'],
            [['user_id', 'type', 'money_amount', 'ball_amount', 'thing_id', 'state', 'action'], 'integer'],
            [['action_at', 'created_at'], 'safe'],
            [['action_prop'], 'string', 'max' => 255],
            [
                ['thing_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Thing::class,
                'targetAttribute' => ['thing_id' => 'id'],
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::class,
                'targetAttribute' => ['user_id' => 'id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'           => Yii::t('app', 'ID'),
            'user_id'      => Yii::t('app', 'User ID'),
            'type'         => Yii::t('app', 'Type'),
            'money_amount' => Yii::t('app', 'Money Amount'),
            'ball_amount'  => Yii::t('app', 'Ball Amount'),
            'thing_id'     => Yii::t('app', 'Thing ID'),
            'state'       => Yii::t('app', 'State'),
            'action'       => Yii::t('app', 'Action'),
            'action_at'    => Yii::t('app', 'Action Time'),
            'action_prop'  => Yii::t('app', 'Action Prop'),
            'created_at'   => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThing()
    {
        return $this->hasOne(Thing::class, ['id' => 'thing_id'])->inverseOf('gifts');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id'])->inverseOf('gifts');
    }

    /**
     * {@inheritdoc}
     * @return GiftQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GiftQuery(get_called_class());
    }
}
