<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%gift_thing}}".
 *
 * @property int    $id
 * @property string $title
 * @property int    $active         Товар участвует в розыгрыше
 * @property int    $count_allowed  Кол-во товара, выделенное для розыгрыша
 * @property int    $count_reserved Кол-во зарезервированного товара (разыграно)
 *
 * @property Gift[] $gifts
 */
class Thing extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%gift_thing}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['count_allowed', 'count_reserved'], 'number', 'min' => 0],
            [['active'], 'boolean'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'             => Yii::t('app', 'ID'),
            'title'          => Yii::t('app', 'Title'),
            'active'         => Yii::t('app', 'Active'),
            'count_allowed'  => Yii::t('app', 'Count Allowed'),
            'count_reserved' => Yii::t('app', 'Count Reserved'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGifts()
    {
        return $this->hasMany(Gift::class, ['thing_id' => 'id'])->inverseOf('thing');
    }

    /**
     * {@inheritdoc}
     * @return ThingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ThingQuery(static::class);
    }
}
