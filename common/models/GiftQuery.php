<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Gift]].
 *
 * @see Gift
 */
class GiftQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Gift[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Gift|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Выделить призы в статусе ожидания действия
     * @return ActiveQuery|static
     */
    public function statusWaiting()
    {
        return parent::where(['state' => Gift::STATE_WAITING]);
    }
}
