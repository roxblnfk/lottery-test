<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\db\Expression;

/**
 * This is the ActiveQuery class for [[Thing]].
 *
 * @see Thing
 */
class ThingQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Thing[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Thing|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Выделить только доступный для розыгрыша товар
     * @return ActiveQuery|static
     */
    public function allowedForGift()
    {
        return parent::where(['active' => true])
              ->andWhere(new Expression('`count_allowed` >  `count_reserved`'));
    }

    /**
     * Получить сумму разниц между доступным и зарезервированным товаром (среди всех выделенных)
     * @return int
     */
    public function sumAvailableThings()
    {
        return parent::sum(new Expression('`count_allowed` -  `count_reserved`'));
    }

    /**
     * Получить случайный товар
     * @return Thing|ActiveRecord
     */
    public function getRandom()
    {
        return parent::orderBy(new Expression('rand()'))->one();
    }
}
