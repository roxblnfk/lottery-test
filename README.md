# Установка

Клонировать репозиторий. Установить зависимости Composer, DEV окружение, настроить DB и накатить миграции
```bash
composer install
yii init
yii migrate
```

## Действия

- Сначала на бумажке спроектировал структуру БД
- Настроив приоложение Yii2 Advanced, инициирровал git
- В командной строке сгенерировал миграции для двух связанных таблиц, вручную откорректировал
- По таблицам в Gii сгенеровал модели
- Для наполнения БД решил в пользу консольных команд. Реализовал самые необходимые базовые, наметив некоторые дальнейшие действия в `todo:`
- Создал класс для работы с призами, перенёс туда соответствующую логику

Осталось добавить функционал для консольной команды из задания **-advanced-**, а также требуемый юнит-тест.

### Новые консольные команды

- `yii user/add $username $email $password` Добавить пользователя
- `yii thing/add $title $count` Добавить запись в список разыгрываемых (подарочных) вещей
- `yii thing/active-list` Вывести список вещей, помеченных галочкой "для розыгрыша"
- `yii gift/add $user $type $value` Назначить пользователю подарок, где\
  `$user` - логин или id пользователя\
  `$type` - тип подарка (1 - деньги, 2 - баллы, 3 - вещь)\
  `$value` - значение (кол-во денег или id вещи)
- `yii gift/send-money` \[в работе\] Отправить денежные поризы в банк
